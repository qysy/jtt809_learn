package com.sjx.jtt809.client;

import cn.hutool.core.thread.ThreadUtil;

public class ClientStartMain {

    public static void main(String[] args) throws Exception {
        // 启动下级平台客户端
        ThreadUtil.execute(new PrimaryLinkClientManager("127.0.0.1", 8080, "127.0.0.1", 32122));
        // 启动下级平台服务端
        ThreadUtil.execute(new SlaveLinkClientManager("127.0.0.1", 32122));
    }
}
