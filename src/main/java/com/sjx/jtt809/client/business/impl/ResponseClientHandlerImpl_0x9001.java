package com.sjx.jtt809.client.business.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.sjx.jtt809.client.business.IClientBusinessServer;
import com.sjx.jtt809.client.pojo.RequestClientJtt809_0x9002;
import com.sjx.jtt809.client.pojo.RequestClientJtt809_0x9002_Result;
import com.sjx.jtt809.client.pojo.ResponseClientJtt809_0x1002;
import com.sjx.jtt809.client.pojo.ResponseClientJtt809_0x9001;
import io.netty.channel.ChannelHandlerContext;

/**
 * 从链路连接保持请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REQ。
 * 描述:从链路建立成功后，上级平台向下级平台发送从链路连接保持请求消息，以保持
 * 从链路的连接状态。
 * 从链路连接保持请求消息，数据体为空。
 */
public class ResponseClientHandlerImpl_0x9001 implements IClientBusinessServer<ResponseClientJtt809_0x9001> {

    private static final Log logger = LogFactory.get();

    @Override
    public void businessHandler(ChannelHandlerContext ctx, ResponseClientJtt809_0x9001 msg) {
        try {
            RequestClientJtt809_0x9002 requestClientJtt8090x9002 = new RequestClientJtt809_0x9002();

            // 判断校验码是否一致
            if (msg.getVerifyCode() >= 0 && msg.getVerifyCode() != ResponseClientJtt809_0x1002.getResultVerifyCode()) {
                requestClientJtt8090x9002.setResult(RequestClientJtt809_0x9002_Result.VERIFY_CODE_ERROR);
            } else if (msg.getVerifyCode() >= 0 && msg.getVerifyCode() == ResponseClientJtt809_0x1002.getResultVerifyCode()){
                requestClientJtt8090x9002.setResult(RequestClientJtt809_0x9002_Result.SUCCESS);
            }  else {
                requestClientJtt8090x9002.setResult(RequestClientJtt809_0x9002_Result.OTHER);
            }
            ctx.writeAndFlush(requestClientJtt8090x9002);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
