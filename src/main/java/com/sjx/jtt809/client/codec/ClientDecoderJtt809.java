package com.sjx.jtt809.client.codec;

import com.sjx.jtt809.client.handler.ResponseClientFactory;
import com.sjx.jtt809.server.pojo.Response;
import com.sjx.jtt809.server.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * jtt809解码类
 */
public class ClientDecoderJtt809 extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int msgId = in.getUnsignedShort(ConstantJtt809Util.MSG_ID_INDEX_OF_PACKAGE);

        try {
            // 工厂根据传入的指令实例化对象
            Response response = ResponseClientFactory.createResponse(msgId, in);
            if (null != response) {
                out.add(response);
            }
        } finally {
            in.clear();
        }
    }
}
