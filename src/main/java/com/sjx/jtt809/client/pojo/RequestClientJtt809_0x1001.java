package com.sjx.jtt809.client.pojo;

import com.sjx.jtt809.server.pojo.BasePackage;
import com.sjx.jtt809.server.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

public class RequestClientJtt809_0x1001 extends BasePackage {

    /**
     * 用户名
     */
    private int userId;

    /**
     * 密码
     */
    private String password;

    /**
     * 下级平台提供对应的从链路服务端 IP 地址
     */
    private String downLinkIp;

    /**
     * 下级平台提供对应的从链路服务器端口号
     */
    private int downLinkPort;

    /**
     * 构造函数
     */
    public RequestClientJtt809_0x1001() {
        super(ConstantJtt809Util.UP_CONNECT_REQ);
        this.msgBodyLength = 46;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDownLinkIp() {
        return downLinkIp;
    }

    public void setDownLinkIp(String downLinkIp) {
        this.downLinkIp = downLinkIp;
    }

    public int getDownLinkPort() {
        return downLinkPort;
    }

    public void setDownLinkPort(int downLinkPort) {
        this.downLinkPort = downLinkPort;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        buf.writeInt(getUserId());
        buf.writeBytes(getBytesWithLengthAfter(8, getPassword().getBytes()));
        buf.writeBytes(getBytesWithLengthAfter(32, getDownLinkIp().getBytes()));
        buf.writeShort(getDownLinkPort());
    }
}
