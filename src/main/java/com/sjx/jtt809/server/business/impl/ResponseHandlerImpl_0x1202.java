package com.sjx.jtt809.server.business.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.SimpleCache;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.sjx.jtt809.server.business.IBusinessServer;
import com.sjx.jtt809.server.pojo.command.RequestJtt809_0x9101;
import com.sjx.jtt809.server.pojo.command.ResponseJtt809_0x1202;
import com.sjx.jtt809.server.util.ConstantJtt809Util;
import com.sjx.util.ConstantUtil;
import com.sjx.util.PropsUtil;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 * <p>
 * 具体描述：
 * 实时上传车辆定位信息消息
 * 子业务类型标识：UP_EXG_MSG_REAL_LOCATION
 * 描述：主要描述车辆的实时定位信息
 */
public class ResponseHandlerImpl_0x1202 implements IBusinessServer<ResponseJtt809_0x1202> {

    private static final Log logger = LogFactory.get();

    private int MAX_COUNT = PropsUtil.getConfigInstance().getInt(ConstantUtil.JTT809_RECEIVE_GPS_OVERFLOW_NOTICE_COUNT);

    private static SimpleCache<String, Object> simpleCache = new SimpleCache<String, Object>();

    @Override
    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1202 msg) {
        try {
            // 获取客户端地址
            InetSocketAddress upRemoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
            String downPlatformKey = ConstantJtt809Util.UP_DOWN_PLATFORM_LINK.get(upRemoteAddress.toString());

            Object cacheObject = simpleCache.get(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_COUNT");

            if (cacheObject == null) {
                // 初始化
                simpleCache.put(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_COUNT", 1);
                simpleCache.put(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_STARTTIME", (System.currentTimeMillis() / 1000));
            } else if (Convert.toInt(cacheObject) == MAX_COUNT) {
                // 获取开始时间
                String startTimeStr = Convert.toStr(simpleCache.get(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_STARTTIME"));
                long startTime = Long.parseLong(startTimeStr);
                // 获取结束时间
                long endTime = System.currentTimeMillis() / 1000;
                // 封装数据包
                RequestJtt809_0x9101 requestJtt8090x9101 = new RequestJtt809_0x9101();
                requestJtt8090x9101.setDynamicInfoTotal(MAX_COUNT);
                requestJtt8090x9101.setStartTime(startTime);
                requestJtt8090x9101.setEndTime(endTime);
                // 发送
                ChannelHandlerContext downPlatformSocket = ConstantJtt809Util.DOWN_PLATFORM.get(downPlatformKey);
                downPlatformSocket.writeAndFlush(requestJtt8090x9101);

                // 重置
                simpleCache.put(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_COUNT", 1);
                simpleCache.put(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_STARTTIME", (System.currentTimeMillis() / 1000));
            } else {
                // 计数
                int count = Convert.toInt(cacheObject);
                simpleCache.put(downPlatformKey + "_" + "UP_EXG_MSG_REAL_LOCATION_COUNT", ++count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
