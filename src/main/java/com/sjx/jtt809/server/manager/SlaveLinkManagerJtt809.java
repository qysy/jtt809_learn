package com.sjx.jtt809.server.manager;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.sjx.jtt809.server.handler.initializer.SlaveLinkServerJtt809Initializer;
import com.sjx.jtt809.server.pojo.command.RequestJtt809_0x9001;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.concurrent.TimeUnit;

/**
 * 从链路启动管理器
 */
public class SlaveLinkManagerJtt809 {

    private static final Log logger = LogFactory.get();

    private Bootstrap bootstrap;

    private Channel channel;

    /**
     * ip
     */
    private String ip;

    /**
     * 端口
     */
    private int port;

    /**
     * 校验码
     */
    private int verifyCode;

    /**
     * 构造函数
     *
     * @param ip
     * @param port
     * @param verifyCode
     */
    public SlaveLinkManagerJtt809(String ip, int port, int verifyCode) {
        this.ip = ip;
        this.port = port;
        this.verifyCode = verifyCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(int verifyCode) {
        this.verifyCode = verifyCode;
    }

    /**
     * 启动客户端
     */
    public Channel start() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .channel(NioSocketChannel.class)
                    .handler(new SlaveLinkServerJtt809Initializer(ip, port, verifyCode));

            // 连接服务端
            ChannelFuture channelFuture = bootstrap.connect(ip, port);
            channelFuture.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (!future.isSuccess()) {
                        final EventLoop loop = future.channel().eventLoop();
                        loop.schedule(new Runnable() {
                            @Override
                            public void run() {
                                logger.info("======> 【从链路|从链路连接保持请求消息】下级平台连接不上，开始重连操作......");
                                start();
                            }
                        }, 5L, TimeUnit.SECONDS);
                    } else {
                        channel = future.channel();
                        logger.info("======> 【从链路|从链路连接保持请求消息】下级平台连接成功......");
                    }
                }
            });

            // 延迟
            Thread.sleep(500);

            // 从链路链接请求
            RequestJtt809_0x9001 requestJtt8090x9001 = new RequestJtt809_0x9001();
            requestJtt8090x9001.setVerifyCode(verifyCode);
            channel.writeAndFlush(requestJtt8090x9001);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return this.channel;
    }
}
