package com.sjx.jtt809.server.pojo.command;

import com.sjx.jtt809.server.pojo.Response;
import io.netty.buffer.ByteBuf;

/**
 * 主链路连接保持请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识:UP_ LINK 下 EST_ REQ。
 * 描述:下级平台向上级平台发送主链路连接保持清求消息，以保持主链路的连接。
 * 主链路连接保持清求消息，数据体为空。
 */
public class ResponseJtt809_0x1005 extends Response {

    @Override
    protected void decodeImpl(ByteBuf buf) {

    }
}
