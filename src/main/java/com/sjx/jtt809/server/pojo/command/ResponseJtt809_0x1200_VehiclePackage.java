package com.sjx.jtt809.server.pojo.command;

import com.sjx.jtt809.server.pojo.Response;
import io.netty.buffer.ByteBuf;

import java.nio.charset.Charset;

/**
 * 车辆信息基本类
 */
public abstract class ResponseJtt809_0x1200_VehiclePackage extends Response {

    /**
     * 车牌号
     */
    private String vehicleNo;

    /**
     * 车辆颜色，按照 JT/T415-2006中 5.4.12 的规定
     * 1 - 蓝色
     * 2 - 黄色
     * 3 - 黑色
     * 4 - 白色
     * 9 - 其他
     */
    private byte vehicleColor;

    /**
     * 子业务类型标识
     */
    private int dataType;

    /**
     * 后续数据长度
     */
    protected int dataLength;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public byte getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(byte vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public int getDataLength() {
        return dataLength;
    }

    public void setDataLength(int dataLength) {
        this.dataLength = dataLength;
    }

    /**
     * 解码车辆基本数据
     *
     * @param buf
     */
    @Override
    protected void decodeImpl(ByteBuf buf) {
        // 车牌
        this.vehicleNo = buf.readBytes(21).toString(Charset.forName("GBK")).trim();
        // 车牌颜色
        this.vehicleColor = buf.readByte();
        // 子业务类型
        this.dataType = buf.readUnsignedShort();
        // 后续数据长度
        this.dataLength = buf.readInt();

        // 解码具体车辆数据实现方法
        decodeDataImpl(buf);
    }

    /**
     * 解码具体车辆数据实现方法
     *
     * @param buf
     */
    protected abstract void decodeDataImpl(ByteBuf buf);

}
